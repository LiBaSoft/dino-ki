"""
This class implements a key trigger.

It can be used to simulate key events useful for our interaction with the game.

Example:
    kt = KeyTrigger()
    ...
    if should_jump:
        kt.trigger_up()
"""

import pyautogui


class KeyTrigger(object):

    def __init__(self, up_key: str="space"):
        self.up_key = up_key

    def trigger_up(self):
        pyautogui.press(self.up_key)
